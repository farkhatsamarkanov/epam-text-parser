package com.epam.javatxt.entity;

import java.util.List;

/**
 * Class {@code Sentence} represents part of {@code Paragraph} class.
 */
public class Sentence extends TextComposite {

    /**
     * Constructs new {@code Sentence} object containing list of {@code SentencePart} as {@code children} list.
     *
     * @param sentenceParts {@code List<SentencePart>} value to store
     */
    public Sentence(List<SentencePart> sentenceParts) {
        sentenceParts.forEach(this::add);
    }
}
