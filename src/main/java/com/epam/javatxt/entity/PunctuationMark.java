package com.epam.javatxt.entity;

import java.util.regex.Pattern;

/**
 * Class {@code PunctuationMark} represents part of {@code Sentence}.
 */
public class PunctuationMark extends SentencePart {

    /**
     * The {@code String} value is used for punctuation mark storage.
     * Not a {@code char} because it's possible for {@code PunctuationMark} object to contain {@code "\r\n"} value.
     */
    private String punctuationMarkString;

    /**
     * Constructs new {@code PunctuationMark} object containing {@code punctuationMarkString} string.
     *
     * @param punctuationMarkString {@code String} value to store
     */
    public PunctuationMark(String punctuationMarkString) {
        this.punctuationMarkString = punctuationMarkString;
    }

    /**
     * Getter for {@code punctuationMarkString} value.
     */
    public String getPunctuationMarkString() {
        return punctuationMarkString;
    }

    @Override
    public int count() {
        return punctuationMarkString.length();
    }

    /**
     * Presents {@code punctuationMarkString} as {@code String}.
     */
    @Override
    public String toString() {
        if (Pattern.matches("[])},;:]", punctuationMarkString)) {
            return punctuationMarkString + " ";
        } else if (Pattern.matches("[\\[({]", punctuationMarkString)) {
            return " " + punctuationMarkString;
        } else {
            return punctuationMarkString;
        }
    }
}
