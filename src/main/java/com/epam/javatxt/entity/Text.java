package com.epam.javatxt.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code Text} represents wrapper of text string.
 */
public class Text extends TextComposite {

    /**
     * Constructs new {@code Text} object containing list of {@code Paragraph} as {@code children} list.
     *
     * @param paragraphs {@code List<Paragraph>} value to store
     */
    public Text(List<Paragraph> paragraphs) {
        paragraphs.forEach(this::add);
    }

    /**
     * Constructs new {@code Text} object as a copy of {@code textToCopy} object
     * iterating over each element of {@code textToCopy} and making copies of them.
     *
     * @param textToCopy {@code Text} value to store
     */
    public Text(Text textToCopy) {
        for (TextComposite paragraph : textToCopy.getChildren()) {
            List<Sentence> paragraphCopy = new ArrayList<>();
            for (TextComposite sentence : paragraph.getChildren()) {
                List<SentencePart> sentenceCopy = new ArrayList<>();
                for (TextComposite sentencePart : sentence.getChildren()) {
                    SentencePart sentencePartCopy;
                    if (sentencePart instanceof Word) {
                        List<Letter> wordCopy = new ArrayList<>();
                        for (TextComposite letter : sentencePart.getChildren()) {
                            wordCopy.add(new Letter(((Letter) letter).getCharacter()));
                        }
                        sentencePartCopy = new Word(wordCopy);
                    } else {
                        String punctuationMarkString = ((PunctuationMark) sentencePart).getPunctuationMarkString();
                        sentencePartCopy = new PunctuationMark(punctuationMarkString);
                    }
                    sentenceCopy.add(sentencePartCopy);
                }
                paragraphCopy.add(new Sentence(sentenceCopy));
            }
            this.add(new Paragraph((paragraphCopy)));
        }
    }
}
