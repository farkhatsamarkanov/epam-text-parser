package com.epam.javatxt.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Abstract class {@code TextComposite} represents composite of text and implements "Composite" design pattern
 */
public abstract class TextComposite {

    /**
     * The {@code children} value is used to store inner {@code TextComposite} objects
     */
    private List<TextComposite> children = new ArrayList<>();

    /**
     * Adds new {@code TextComposite} object to {@code children} list
     *
     * @param textComposite {@code TextComposite} value to store
     */
    public void add(TextComposite textComposite) {
        children.add(textComposite);
    }

    /**
     * Getter for {@code children} value
     */
    public List<TextComposite> getChildren() {
        return children;
    }

    /**
     * Returns number of inner {@code TextComposite} objects
     */
    public int count() {
        return children.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TextComposite)) return false;
        TextComposite that = (TextComposite) o;
        return Objects.equals(children, that.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(children);
    }
}
