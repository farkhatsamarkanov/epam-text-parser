package com.epam.javatxt.entity;

/**
 * Abstract class {@code SentencePart} represents part of {@code Sentence} class:
 * ({@code Word} or {@code PunctuationMark}).
 * This wrapper class is needed to ensure that {@code Sentence} object contains only {@code SentencePart} objects
 * instead of {@code TextComposite} objects.
 */
public abstract class SentencePart extends TextComposite {
}


