package com.epam.javatxt.entity;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class {@code Word} represents part of {@code Sentence} class.
 */
public class Word extends SentencePart {

    /**
     * Constructs new {@code Word} object containing list of {@code Letter} as {@code children} list.
     *
     * @param letters
     *         {@code List<Letter>} value to store
     */
    public Word(List<Letter> letters) {
        letters.forEach(this::add);
    }

    /**
     * Constructs new {@code Word} object containing sequence of {@code char}.
     * Each {@code char} is wrapped as {@code Letter} object and added to {@code children} list.
     *
     * @param letters
     *         {@code char} sequence to store
     */
    public Word(char... letters) {
        for (char letter : letters) {
            this.add(new Letter(letter));
        }
    }

    /**
     * Presents each {@code Letter} object of {@code children} list as {@code String}.
     */
    @Override
    public String toString() {
        return getChildren()
                .stream()
                .map(TextComposite::toString)
                .collect(Collectors.joining());
    }
}
