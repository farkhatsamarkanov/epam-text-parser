package com.epam.javatxt.entity;

/**
 * Class {@code Letter} represents part of {@code Word}, contains {@code character} storage.
 */
public class Letter extends TextComposite {

    /**
     * The value is used for {@code char} storage.
     */
    private char character;

    /**
     * Constructs new {@code Letter} object containing {@code character} {@code char}.
     *
     * @param character {@code char} value to store
     */
    public Letter(char character) {
        this.character = character;
    }

    /**
     * Getter for {@code character} value.
     */
    public char getCharacter() {
        return character;
    }

    @Override
    public int count() {
        return 1;
    }

    @Override
    public String toString() {
        return String.valueOf(character);
    }
}
