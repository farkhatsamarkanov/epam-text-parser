package com.epam.javatxt.entity;

import java.util.List;

/**
 * Class {@code Paragraph} represents part of {@code Text} class.
 */
public class Paragraph extends TextComposite {

    /**
     * Constructs new {@code Paragraph} object containing list of {@code Sentence} as {@code children} list.
     *
     * @param sentences {@code List<Sentence>} value to store
     */
    public Paragraph(List<Sentence> sentences) {
        sentences.forEach(this::add);
    }
}
