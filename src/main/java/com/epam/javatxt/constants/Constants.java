package com.epam.javatxt.constants;

/**
 * Enum {@code Constants} contains constant values necessary for app, such as
 * splitting regular expressions, line separator and file paths.
 */
public enum Constants {
    LINE_SEPARATOR(System.getProperty("line.separator")),
    TEXT_TO_PARAGRAPHS_SPLIT_REGEX("(?m)(?=^\\s{2})"),
    PARAGRAPH_TO_SENTENCES_SPLIT_REGEX("(?<=(?<![A-Z])\\.)"),
    SENTENCE_TO_WORDS_SPLIT_REGEX("\\s"),
    INPUT_TEXT_PATH("input/input-text.txt"),
    PARSED_UNSORTED_TEXT_PATH("parsed/parsed-unsorted.txt"),
    PARSED_SORTED_PARAGRAPHS_TEXT_PATH("parsed/parsed-sorted-paragraphs.txt"),
    PARSED_SORTED_SENTENCES_TEXT_PATH("parsed/parsed-sorted-sentences.txt"),
    PARSED_SORTED_WORDS_TEXT_PATH("parsed/parsed-sorted-words.txt");

    /**
     * The value is used for {@code String} storage
     */
    private final String value;

    /**
     * Constructs new {@code Constants} enum containing {@code value} {@code String}
     *
     * @param value {@code String} value to store
     */
    Constants(String value) {
        this.value = value;
    }

    /**
     * Getter for {@code value} value
     */
    public String getValue() {
        return value;
    }
}
