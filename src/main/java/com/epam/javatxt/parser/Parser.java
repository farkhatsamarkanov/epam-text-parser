package com.epam.javatxt.parser;

import com.epam.javatxt.entity.TextComposite;
import com.epam.javatxt.exceptions.TextParsingException;

/**
 * Abstract class {@code Parser} is implementation of "Chain of Responsibility" design pattern.
 */
public abstract class Parser {
    /**
     * The value is used for linking to another {@code Parser} object in chain.
     */
    private Parser next;

    /**
     * Constructs new {@code Parser} object containing link to next {@code Parser} object in chain.
     *
     * @param next
     *         reference to next {@code Parser} object
     */
    Parser(Parser next) {
        this.next = next;
    }

    /**
     * Parses {@code String} of text (or part of text) and wraps it in corresponding {@code TextComposite} object
     *
     * @return if {@code next} has a reference to another {@code Parser} object, returns result of {@code handleParsing}
     *          method in it. If not, returns {@code null}.
     *
     * @throws TextParsingException
     *          If text parsing is failed.
     */
    public TextComposite handleParsing(String textToParse) throws TextParsingException {
        if (next != null) {
            return next.handleParsing(textToParse);
        } else {
            return null;
        }
    }
}
