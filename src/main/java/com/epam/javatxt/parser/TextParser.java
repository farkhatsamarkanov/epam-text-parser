package com.epam.javatxt.parser;

import com.epam.javatxt.constants.Constants;
import com.epam.javatxt.entity.Paragraph;
import com.epam.javatxt.entity.Text;
import com.epam.javatxt.exceptions.TextParsingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Class {@code TextParser} parses text {@code String} and wraps it in {@code Text} object.
 */
public class TextParser extends Parser {

    /**
     * Instance of Log4J2 logger
     */
    private final static Logger LOG = LogManager.getLogger("parser");

    /**
     * Constructs new {@code TextParser} with link to another {@code Parser} object.
     *
     * @param next
     *         link to {@code Parser} object to store
     */
    TextParser(Parser next) {
        super(next);
    }

    /**
     * Parses text string {@code textToParse}
     *
     * @param textToParse
     *         text string to parse
     */
    @Override
    public Text handleParsing(String textToParse) throws TextParsingException {
        // Compiling pattern using splitting regex
        Pattern pattern = Pattern.compile(Constants.TEXT_TO_PARAGRAPHS_SPLIT_REGEX.getValue());
        // Splitting text string into array of paragraph strings
        String[] paragraphStrings = pattern.split(textToParse);
        // Instantiating new ArrayList to store parsed paragraphs
        List<Paragraph> listOfParagraphs = new ArrayList<>();
        // If array of paragraph strings is not empty
        if (paragraphStrings.length != 0) {
            for (int i = 0; i < paragraphStrings.length; i++) {
                // Trim each paragraph string
                paragraphStrings[i] = paragraphStrings[i].trim();
                try {
                    // Parse each paragraph string
                    listOfParagraphs.add(Objects.requireNonNull((Paragraph) super.handleParsing(paragraphStrings[i])));
                } catch (NullPointerException e) {
                    LOG.error("There is no paragraph parser linked to text parser " + e);
                }
            }
            LOG.info("Text parsing is complete");
            // Return new Text object containing listOfParagraphs ArrayList
            return new Text(listOfParagraphs);
        } else {
            throw new TextParsingException("Failed to split text into paragraphs");
        }
    }
}
