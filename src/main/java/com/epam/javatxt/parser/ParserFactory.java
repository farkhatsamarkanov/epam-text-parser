package com.epam.javatxt.parser;

/**
 * Class {@code ParserFactory} is implementation of "Factory" design pattern.
 */
public class ParserFactory {

    /**
     * Builds new text parsing chain of {@code Parser} objects
     *
     * @return Returns new {@code TextParser} object linked to {@code ParagraphParser} object
     *          which is linked to {@code SentenceParser} object
     */
    public static TextParser buildTextParserChain() {
        return new TextParser(new ParagraphParser(new SentenceParser(null)));
    }
}
