package com.epam.javatxt.parser;

import com.epam.javatxt.constants.Constants;
import com.epam.javatxt.entity.Paragraph;
import com.epam.javatxt.entity.Sentence;
import com.epam.javatxt.exceptions.TextParsingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Class {@code ParagraphParser} parses paragraph {@code String} and wraps it in {@code Paragraph} object.
 */
public class ParagraphParser extends Parser {
    private final static Logger LOG = LogManager.getLogger("parser");

    /**
     * Constructs new {@code ParagraphParser} with link to another {@code Parser} object.
     *
     * @param next
     *         link to {@code Parser} object to store
     */
    ParagraphParser(Parser next) {
        super(next);
    }

    /**
     * Parses paragraph string {@code paragraphToParse}
     *
     * @param paragraphToParse
     *         paragraph string to parse
     */
    @Override
    public Paragraph handleParsing(String paragraphToParse) throws TextParsingException {
        // Compiling pattern using splitting regex
        Pattern pattern = Pattern.compile(Constants.PARAGRAPH_TO_SENTENCES_SPLIT_REGEX.getValue());
        // Splitting paragraph string into array of sentence strings
        String[] sentenceStrings = pattern.split(paragraphToParse);
        List<Sentence> listOfSentences = new ArrayList<>();
        if (sentenceStrings.length != 0) {
            for (String sentenceString : sentenceStrings) {
                try {
                    listOfSentences.add(Objects.requireNonNull((Sentence) super.handleParsing(sentenceString.trim())));
                } catch (NullPointerException e) {
                    LOG.error("There is no sentence parser linked to paragraph parser " + e);
                }
            }
            return new Paragraph(listOfSentences);
        } else {
            throw new TextParsingException("Failed to split paragraph into sentences");
        }
    }
}
