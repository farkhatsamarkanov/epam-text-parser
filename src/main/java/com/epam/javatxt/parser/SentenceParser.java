package com.epam.javatxt.parser;

import com.epam.javatxt.constants.Constants;
import com.epam.javatxt.entity.*;
import com.epam.javatxt.exceptions.TextParsingException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Class {@code SentenceParser} parses sentence {@code String} and wraps it in {@code Sentence} object.
 */
public class SentenceParser extends Parser {

    /**
     * Constructs new {@code SentenceParser} with link to another {@code Parser} object.
     *
     * @param next
     *         link to {@code Parser} object to store
     */
    SentenceParser(Parser next) {
        super(next);
    }

    /**
     * Parses sentence string {@code sentenceToParse}
     *
     * @param sentenceToParse
     *         sentence string to parse
     */
    @Override
    public Sentence handleParsing(String sentenceToParse) throws TextParsingException {
        // Splitting regex pattern
        Pattern pattern = Pattern.compile(Constants.SENTENCE_TO_WORDS_SPLIT_REGEX.getValue());
        // List of sentence parts
        List<SentencePart> listOfWordsAndPunctuationMarks = new ArrayList<>();
        // Splitting sentence string to sentence part strings
        String[] sentencePartStrings = pattern.split(sentenceToParse);
        // Temporary StringBuilder to separate each sentence part string
        StringBuilder separatingStringBuilder = new StringBuilder();
        // Iterating over sentencePartStrings array
        if (sentencePartStrings.length != 0) {
            for (String sentencePartString : sentencePartStrings) {
                // After trimming of paragraph string \r\n line separators inside of sentences become "" strings
                // If sentence part string is "" then wrap it as PunctuationMark object containing line separator
                if (sentencePartString.equals("")) {
                    listOfWordsAndPunctuationMarks.add(new PunctuationMark(Constants.LINE_SEPARATOR.getValue()));
                } else {
                    // Array of char to contain separate sentence part
                    char[] characters = sentencePartString.toCharArray();
                    // Iterating over each character of sentence part string
                    for (int i = 0; i < characters.length; i++) {
                        // If character is letter of digit
                        if (Character.isLetterOrDigit(characters[i])) {
                            // Append it to temporary StringBuilder
                            separatingStringBuilder.append(characters[i]);
                            // If character is the last character in string
                            if (i == characters.length - 1) {
                                // Wrap characters in temporary StringBuilder as Word object
                                listOfWordsAndPunctuationMarks.add(new Word(separatingStringBuilder.toString().toCharArray()));
                                // Flushing temporary StringBuilder by referencing it to new StringBuilder object
                                separatingStringBuilder = new StringBuilder();
                            }
                        } else {
                            // If character is not letter or digit
                            // And if separatingStringBuilder contains characters, wrap them as Word object
                            if (separatingStringBuilder.length() != 0) {
                                listOfWordsAndPunctuationMarks.add(new Word(separatingStringBuilder.toString().toCharArray()));
                                // Flush separatingStringBuilder
                                separatingStringBuilder = new StringBuilder();
                            }
                            // Wrap character as PunctuationMark object
                            listOfWordsAndPunctuationMarks.add(new PunctuationMark(String.valueOf(characters[i])));
                        }
                    }
                }
            }
            return new Sentence(listOfWordsAndPunctuationMarks);
        } else {
            throw new TextParsingException("Failed to split sentence into words and signs");
        }
    }
}
