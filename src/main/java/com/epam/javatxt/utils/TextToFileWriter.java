package com.epam.javatxt.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class {@code TextToFileWriter} writes text {@code String} to file.
 */
public class TextToFileWriter {

    private final static Logger LOG = LogManager.getLogger(TextToFileWriter.class);

    /**
     * Writes text {@code String} to {@code fileName} file
     *
     * @param textToWrite
     *         text {@code String} to write.
     * @param fileName
     *         file to write text string to.
     */
    public static void writeParsedTextToFile(String textToWrite, String fileName) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, false));
            writer.write(textToWrite);
            LOG.info("Text written to file: " + fileName);
        } catch (IOException e) {
            LOG.error(e);
        } finally {
            try {
                assert writer != null;
                writer.close();
            } catch (IOException e) {
                LOG.error("Failed to close buffered writer " + e);
            }
        }
    }
}
