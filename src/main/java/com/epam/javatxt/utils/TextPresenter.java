package com.epam.javatxt.utils;

import com.epam.javatxt.constants.Constants;
import com.epam.javatxt.entity.PunctuationMark;
import com.epam.javatxt.entity.SentencePart;
import com.epam.javatxt.entity.Text;
import com.epam.javatxt.entity.TextComposite;
import com.epam.javatxt.entity.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class {@code TextPresenter} converts {@code Text} object to {@code String}.
 */
public class TextPresenter {

    private final static Logger LOG = LogManager.getLogger(TextPresenter.class);

    /**
     * Enum {@code TypeOfText} to distinguish if text is sorted
     */
    public enum TypeOfText {
        UNSORTED,
        SORTED_PARAGRAPHS,
        SORTED_SENTENCES,
        SORTED_WORDS;
    }

    /**
     * Presents {@code Text} object as {@code String}
     *
     * @param textToPresent
     *         {@code Text} object to present {@code String}
     * @param typeOfText
     *         {@code TypeOfText} enum containing type of text
     */
    public static String presentTextAsString(Text textToPresent, TypeOfText typeOfText) {
        StringBuilder temporaryStringBuilder = new StringBuilder();
        for (TextComposite paragraph : textToPresent.getChildren()) {
            temporaryStringBuilder.append(presentParagraphAsString(paragraph, typeOfText));
        }
        LOG.info(typeOfText + " text presented as String: \n" + temporaryStringBuilder.toString());
        return temporaryStringBuilder.toString();
    }

    private static String presentParagraphAsString(TextComposite paragraph, TypeOfText typeOfText) {
        StringBuilder paragraphStringBuilder = new StringBuilder("  ");
        for (TextComposite sentence : paragraph.getChildren()) {
            paragraphStringBuilder.append(presentSentenceAsString(sentence, typeOfText));

        }
        // If text paragraphs are sorted add number of sentences in paragraph at the end of sentence
        if (typeOfText == TypeOfText.SORTED_PARAGRAPHS) {
            paragraphStringBuilder
                    .append(" {")
                    .append(paragraph.count())
                    .append(" sentences}")
                    .append(Constants.LINE_SEPARATOR.getValue());
        } else if (typeOfText == TypeOfText.SORTED_SENTENCES || typeOfText == TypeOfText.UNSORTED) {
            paragraphStringBuilder.append(Constants.LINE_SEPARATOR.getValue());
        }
        return paragraphStringBuilder.toString();
    }

    private static String presentSentenceAsString(TextComposite sentence, TypeOfText typeOfText) {
        // Inserting space at the beginning of each sentence
        StringBuilder sentenceStringBuilder = new StringBuilder(" ");
        for (int i = 0; i < sentence.getChildren().size(); i++) {
            int apostropheCount = 0;
            SentencePart currentSentencePart = (SentencePart) sentence.getChildren().get(i);
            if (currentSentencePart instanceof Word) {
                sentenceStringBuilder.append(currentSentencePart);
                // Inserting space between Word objects
                if (i != sentence.getChildren().size() - 1 && sentence.getChildren().get(i + 1) instanceof Word) {
                    sentenceStringBuilder.append(" ");
                }
                // If sentence words are sorted add number of letters in word at the end of word
                if (typeOfText == TypeOfText.SORTED_WORDS) {
                    sentenceStringBuilder
                            .append("{")
                            .append(currentSentencePart.count())
                            .append("} ");
                }
            } else if (currentSentencePart instanceof PunctuationMark) {
                // If current sentence part is apostrophe
                if (((PunctuationMark) currentSentencePart).getPunctuationMarkString().equals("'")) {
                    apostropheCount++;
                    // If number of apostrophes is even
                    if (apostropheCount % 2 == 0) {
                        // Inserting apostrophe in string
                        sentenceStringBuilder.append(currentSentencePart);
                        // If there is a word after apostrophe, inserting space after apostrophe
                        if (i != sentence.getChildren().size() - 1 && sentence.getChildren().get(i + 1) instanceof Word) {
                            sentenceStringBuilder.append(" ");
                        }
                    } else {
                        // If number of apostrophes is odd, first insert space then insert apostrophe to string
                        sentenceStringBuilder.append(" ").append(currentSentencePart);
                    }
                } else {
                    sentenceStringBuilder.append(currentSentencePart);
                }
            }
        }
        // If paragraph sentences are sorted add number of words in sentence at the end of sentence
        if (typeOfText == TypeOfText.SORTED_SENTENCES) {
            sentenceStringBuilder
                    .append(" {")
                    .append(sentence.count())
                    .append(" words}");
        } else if (typeOfText == TypeOfText.SORTED_WORDS) {
            sentenceStringBuilder.append(Constants.LINE_SEPARATOR.getValue());
        }
        return sentenceStringBuilder.toString();
    }
}
