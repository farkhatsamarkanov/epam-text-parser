package com.epam.javatxt.utils;

import com.epam.javatxt.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;

/**
 * Class {@code TextSorter} sorts {@code Text} parts.
 */
public class TextSorter {

    private final static Logger LOG = LogManager.getLogger(TextSorter.class);

    /**
     * Sorts {@code Text} object's paragraphs by number of sentences in them.
     *
     * @param textToSort
     *         {@code Text} object to sort.
     */
    public static Text sortParagraphsBySentencesNumber(Text textToSort) {
        Text textToReturn = new Text(textToSort);
        textToReturn.getChildren().sort(Comparator.comparing(TextComposite::count));
        LOG.info("Text paragraphs sorted by number of sentences");
        return textToReturn;
    }

    /**
     * Sorts {@code Text} object's sentences by number of words in them.
     *
     * @param textToSort
     *         {@code Text} object to sort.
     */
    public static Text sortSentencesByWordsNumber(Text textToSort) {
        Text textToReturn = new Text(textToSort);
        for (TextComposite paragraph : textToReturn.getChildren()) {
            paragraph.getChildren().sort(Comparator.comparing(TextComposite::count));
        }
        LOG.info("Text sentences sorted by number of words");
        return textToReturn;
    }

    /**
     * Sorts {@code Text} object's words by number of letters in them.
     *
     * @param textToSort
     *         {@code Text} object to sort.
     */
    public static Text sortWordsByLength(Text textToSort) {
        Text textToReturn = new Text(textToSort);
        for (TextComposite paragraph : textToReturn.getChildren()) {
            for (TextComposite sentence : paragraph.getChildren()) {
                sentence.getChildren().removeIf(sentencePart -> sentencePart instanceof PunctuationMark);
                sentence.getChildren().sort(Comparator.comparing(TextComposite::count));
            }
        }
        LOG.info("Text words sorted by length");
        return textToReturn;
    }
}
