package com.epam.javatxt.utils;

import com.epam.javatxt.constants.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Class {@code TextFileReader} reads text {@code String} from file.
 */
public class TextFileReader {

    private final static Logger LOG = LogManager.getLogger(TextFileReader.class);

    /**
     * Reads text string from file
     *
     * @param fileName
     *         file with text
     */
    public static String readFile(String fileName) {
        // Temporary StringBuilder to build a text string
        StringBuilder textStringBuilder = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8));
            String line;
            while ((line = reader.readLine()) != null) {
                textStringBuilder.append(line).append(Constants.LINE_SEPARATOR.getValue());
            }
        } catch (FileNotFoundException e) {
            LOG.error("File " + fileName + " not found! " + e);
        } catch (IOException e) {
            LOG.error("File error or I/O error: " + e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                LOG.error("Unable to close BufferedReader: " + e);
            }
        }
        if (!textStringBuilder.toString().equals("")) {
            return textStringBuilder.toString();
        } else {
            LOG.debug("Text file is empty!");
            return null;
        }

    }
}
