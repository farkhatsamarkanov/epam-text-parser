package com.epam.javatxt.initialization;

import com.epam.javatxt.constants.Constants;
import com.epam.javatxt.entity.Text;
import com.epam.javatxt.exceptions.TextParsingException;
import com.epam.javatxt.parser.ParserFactory;
import com.epam.javatxt.parser.TextParser;
import com.epam.javatxt.utils.TextFileReader;
import com.epam.javatxt.utils.TextPresenter;
import com.epam.javatxt.utils.TextSorter;
import com.epam.javatxt.utils.TextToFileWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

public class App {
    private final static Logger LOG = LogManager.getLogger(App.class);
    public static void main(String[] args) {
        TextParser textParser = ParserFactory.buildTextParserChain();
        try {
            Text parsedText = textParser.handleParsing(Objects.requireNonNull(TextFileReader.readFile(Constants.INPUT_TEXT_PATH.getValue())));
            TextToFileWriter.writeParsedTextToFile(TextPresenter.presentTextAsString(parsedText, TextPresenter.TypeOfText.UNSORTED), Constants.PARSED_UNSORTED_TEXT_PATH.getValue());
            TextToFileWriter.writeParsedTextToFile(TextPresenter.presentTextAsString(TextSorter.sortParagraphsBySentencesNumber(parsedText), TextPresenter.TypeOfText.SORTED_PARAGRAPHS), Constants.PARSED_SORTED_PARAGRAPHS_TEXT_PATH.getValue());
            TextToFileWriter.writeParsedTextToFile(TextPresenter.presentTextAsString(TextSorter.sortSentencesByWordsNumber(parsedText), TextPresenter.TypeOfText.SORTED_SENTENCES), Constants.PARSED_SORTED_SENTENCES_TEXT_PATH.getValue());
            TextToFileWriter.writeParsedTextToFile(TextPresenter.presentTextAsString(TextSorter.sortWordsByLength(parsedText), TextPresenter.TypeOfText.SORTED_WORDS), Constants.PARSED_SORTED_WORDS_TEXT_PATH.getValue());
        } catch (TextParsingException e) {
            LOG.error("Failed to parse text: " + e);
        }
    }
}
