package com.epam.javatxt.exceptions;

/**
 * Class {@code TextParsingException} represents {@code Exception} which is being thrown
 * when text parsing is failed.
 */
public class TextParsingException extends Exception {
    public TextParsingException() {
    }

    public TextParsingException(String message) {
        super(message);
    }

    public TextParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public TextParsingException(Throwable cause) {
        super(cause);
    }
}
